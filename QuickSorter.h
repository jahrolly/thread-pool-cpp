#pragma once

#include "ThreadPool.h"

#include <list>

template <typename Type = int>
class QuickSorter
{
	using Container = std::list<Type>;
public:
	Container sort(Container& input)
	{
		if (input.empty())
			return input;

		Container result;

		const auto first = input.begin(), last = input.end();
		const auto pivotIt = std::next(first, std::distance(first, last) / 2);
		const auto pivotValue = *pivotIt;

		result.splice(result.begin(), input, pivotIt);

		const auto dividePointIt = std::partition(input.begin(), input.end(),
			[pivotValue](const auto& value) {return value < pivotValue; });

		Container lowerChunk;
		lowerChunk.splice(lowerChunk.end(), input, input.begin(), dividePointIt);
		Container& higherChunk = input;

		Container sortedLowerChunk(sort(lowerChunk));
		Container sortedHigherChunk(sort(higherChunk));

		result.splice(result.end(), sortedHigherChunk);
		result.splice(result.begin(), sortedLowerChunk);

		return result;
	}
};