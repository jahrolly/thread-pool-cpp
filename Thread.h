#pragma once

#include <condition_variable>
#include <thread>

class ThreadPool;

class Thread
{
public:
	Thread(ThreadPool& threadPool);
	void join() { _thread.join(); }
private:
	void run(ThreadPool& threadPool);

	std::thread _thread;
};