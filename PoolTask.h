#pragma once

#include <memory>


class PoolTask
{
	struct functionImplBase
	{
		virtual void call() = 0;
		virtual ~functionImplBase() {}
	};
	std::unique_ptr<functionImplBase> impl;

	template <typename Func>
	struct impl_type : functionImplBase
	{
		Func func;
		impl_type(Func&& func) : func(std::move(func)) {}
		void call() { func(); }
	};
	
public:
	template <typename Func>
	PoolTask(Func&& func) : 
		impl(new impl_type<Func>(std::move(func))) {}
	void operator()() { impl->call(); }
	PoolTask() = default;
	PoolTask(PoolTask&& other) : impl(std::move(other.impl)) {}
	PoolTask& operator=(PoolTask&& other)
	{
		impl = std::move(other.impl);
		return *this;
	}
    PoolTask(const PoolTask&) = delete;
    PoolTask(PoolTask&) = delete;
    PoolTask& operator=(const PoolTask&) = delete;
};
