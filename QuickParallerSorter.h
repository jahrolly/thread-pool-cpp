#pragma once

#include "ThreadPool.h"

#include <list>

template <typename T>
void print(const std::list<T>& input)
{
	std::cout << std::endl;
	for (auto elem : input)
		std::cout << elem << " ";
	std::cout << std::endl;
}

template <typename Type = int>
class QuickParallerSorter
{
	using Container = std::list<Type>;
public:
	Container sort(Container& input)
	{
		if (input.empty())
			return input;

		Container result;

		const auto first = input.begin(), last = input.end();
		const auto pivotIt = std::next(first, std::distance(first, last) / 2);
		const auto pivotValue = *pivotIt;

		result.splice(result.begin(), input, pivotIt);

		const auto dividePointIt = std::partition(input.begin(), input.end(),
			[pivotValue](const auto& value) {return value < pivotValue; });

		Container lowerChunk;
		lowerChunk.splice(lowerChunk.end(), input, input.begin(), dividePointIt);
		Container& higherChunk = input;

		std::future<Container> sortedLowerChunk = _threadPool.submitTask(
			std::bind(&QuickParallerSorter::sort, this, std::move(lowerChunk)));
		Container sortedHigherChunk(sort(higherChunk));


		while (sortedLowerChunk.wait_for(std::chrono::seconds(0)) == std::future_status::timeout) {
			_threadPool.processPendingTasks(); 
		}

		result.splice(result.end(), sortedHigherChunk);
		result.splice(result.begin(), sortedLowerChunk.get());
		return result;
	}

private:
	ThreadPool _threadPool;
};