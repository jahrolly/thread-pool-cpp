#include "Thread.h"
#include "ThreadPool.h"

#include <iostream>
#include <mutex>

Thread::Thread(ThreadPool& threadPool) : _thread([&] { this->run(threadPool); })
{
}



void
Thread::run(ThreadPool& threadPool)
{
	auto& queue = threadPool._queue;
	std::unique_lock<std::mutex> lock(threadPool._poolMutex, std::defer_lock);
	while (!threadPool._abortState)
	{
		lock.lock();
		threadPool._cv.wait(lock, [&] { return !queue.empty(); });
		if (threadPool._abortState)
			return;
		auto task = std::move(queue.popUnlockGet(lock));
		task();
	}
}