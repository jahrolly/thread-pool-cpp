#include <iostream>
#include <chrono>
#include <random>

#include "QuickSorter.h"
#include "QuickParallerSorter.h"

void print(const std::list<int>& input)
{
	std::cout << std::endl;
	for (auto elem : input)
		std::cout << elem << " ";
	std::cout << std::endl;
}

static std::list<int> generate_data(size_t size) {
	using value_type = int;
	// We use static in order to instantiate the random engine
	// and the distribution once only.
	// It may provoke some thread-safety issues.
	static std::uniform_int_distribution<value_type> distribution(
		std::numeric_limits<value_type>::min(),
		std::numeric_limits<value_type>::max());
	static std::default_random_engine generator;

	std::list<value_type> data(size);
	std::generate(data.begin(), data.end(),
		[]() { return distribution(generator); });
	return data;
}

int main() {

	std::list<int> listToSort = generate_data(50000);
	QuickParallerSorter<int> quickSorter;
	std::cout << "Is sorted: "
		<< std::is_sorted(std::begin(listToSort), std::end(listToSort)) << '\n';
	const auto t1 = std::chrono::high_resolution_clock::now();
	const auto sortedList = quickSorter.sort(listToSort);
	const auto t2 = std::chrono::high_resolution_clock::now();
	const auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();

	std::cout << "Is sorted: "
		<< std::is_sorted(std::begin(sortedList), std::end(sortedList)) << '\n';
	std::cout << "In time: "
		<< duration << " [ms] \n";

	std::getchar();
}