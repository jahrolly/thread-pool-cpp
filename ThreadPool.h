#pragma once

#include "Thread.h"
#include "TaskQueue.h"
#include "PoolTask.h"

#include <algorithm>
#include <atomic>
#include <condition_variable>
#include <future>
#include <thread>
#include <queue>
#include <vector>
#include <mutex>
#include <type_traits>

class ThreadPool
{
public:
	using Thread = std::thread;
	using PoolTaskQueue = TaskQueue<PoolTask>;
	friend class Thread;

	ThreadPool(const uint32_t poolSize = std::thread::hardware_concurrency()) :
		_poolSize(poolSize)
	{
		run();
	}

	~ThreadPool()
	{
		_abortState = true;
		_cv.notify_all();
		std::for_each(_threads.begin(), 
			          _threads.end(), 
			          [](auto& thread) {thread.join(); });
	}

	template<class FunctionType>
	std::future<typename std::result_of<FunctionType()>::type> submitTask(FunctionType f)
	{
		using ReturnType = typename std::result_of<FunctionType()>::type;
		std::packaged_task<ReturnType()> task(std::move(f));
		std::future<ReturnType> returnValue(task.get_future());


		std::unique_lock<std::mutex> lock(_poolMutex);
		_queue.emplace(std::move(task));
		_cv.notify_one();
		lock.unlock();

		return returnValue;
	}


	void processPendingTasks()
	{
		std::unique_lock<std::mutex> lock(_poolMutex);
		if (!_queue.empty()) 
		{
			auto task = std::move(_queue.popUnlockGet(lock));
			task();
		}
		return;
	}

private:
	void run()
	{
		for (uint32_t threadNumber{ 0 }; threadNumber < _poolSize; ++threadNumber)
			_threads.emplace_back(std::thread(&ThreadPool::runSingleThread, this));
	}

	void
	runSingleThread()
	{
		std::unique_lock<std::mutex> lock(_poolMutex, std::defer_lock);
		while (!_abortState)
		{
			lock.lock();
			_cv.wait(lock, [&] { return !_queue.empty(); });
			if (_abortState)
				return;
			auto task = std::move(_queue.popUnlockGet(lock));
			task();
		}  
	}


	std::atomic_bool _abortState{ false };
	std::condition_variable _cv;
	std::mutex _poolMutex;
	PoolTaskQueue _queue;
	std::vector<Thread> _threads;
	const uint32_t _poolSize;
};
