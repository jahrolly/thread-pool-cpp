#pragma once

#include <queue>

template <typename Task>
class TaskQueue
{
public:
	void emplace(Task&& task)
	{
		_tasksQueue.emplace(std::move(task));
	}

	Task popUnlockGet(std::unique_lock<std::mutex>& lock)
	{
		auto frontTask = std::move(_tasksQueue.front());
		_tasksQueue.pop();
		lock.unlock();
		return frontTask;
	}

	bool empty() const { return _tasksQueue.empty(); }

private:
	std::queue<Task> _tasksQueue;
};
